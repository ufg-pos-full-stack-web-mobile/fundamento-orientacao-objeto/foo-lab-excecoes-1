/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/05/17.
 */
public interface Calculadora {

    double somar();

    double subtrair();

    double multiplicar();

    double dividir() throws DivisaoPorZeroException;
}
