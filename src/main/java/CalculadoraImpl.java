/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/05/17.
 */
public class CalculadoraImpl implements Calculadora {

    private final double num1;
    private final double num2;

    public CalculadoraImpl(double num1, double num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public double somar() {
        return num1 + num2;
    }

    public double subtrair() {
        return num1 - num2;
    }

    public double multiplicar() {
        return num1 * num2;
    }

    public double dividir() throws DivisaoPorZeroException {
        if (num2 == 0) {
            throw new DivisaoPorZeroException("Denominador não pode ser zero");
        }

        return num1 / num2;
    }
}
