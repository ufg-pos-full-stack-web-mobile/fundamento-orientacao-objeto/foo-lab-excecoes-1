/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/05/17.
 */
public class DivisaoPorZeroException extends Exception {

    public DivisaoPorZeroException(String message) {
        super(message);
    }
}
