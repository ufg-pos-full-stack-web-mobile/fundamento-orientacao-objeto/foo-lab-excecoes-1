import javax.swing.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/05/17.
 */
public class Principal {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String... args) {

        double num1 = capturarDouble("num1");
        double num2 = capturarDouble("num2");

        Calculadora calc = new CalculadoraImpl(num1, num2);

        final StringBuilder sb = new StringBuilder();
        sb.append("Soma: " + calc.somar() + "\n");
        sb.append("Subtração: " + calc.subtrair() + "\n");
        sb.append("Multiplicação: " + calc.multiplicar() + "\n");

        try {
            String message = "Dividir: " + calc.dividir() + "\n";
            sb.append(message);
        } catch (DivisaoPorZeroException e) {
            sb.append(e.getMessage() + "\n");
        }

        JOptionPane.showMessageDialog(null, sb.toString());
    }

    private static double capturarDouble(String nomeVariavel) {
        boolean valido = false;
        double valor = 0;

        do {
            try {
                String numStr = JOptionPane.showInputDialog("Digite o " + nomeVariavel);
                valor = Double.parseDouble(numStr);
                valido = true;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Valor informado não corresponde a um double válido. Tente de novo!");
            }
        } while(!valido);

        return valor;
    }

}
